package com.example.noteapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NoteViewModel: ViewModel() {
    private var _titles = MutableLiveData<MutableList<String>>()
    val titles: LiveData<MutableList<String>> get() = _titles

    fun addTitles(title: String) {
        val tempTitles = if (_titles.value == null) mutableListOf<String>() else _titles.value
        tempTitles?.add(title)
        _titles.value = tempTitles!!
    }
}