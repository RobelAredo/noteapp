package com.example.noteapp.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.example.noteapp.databinding.FragmentNoteBinding
import com.example.noteapp.viewmodel.NoteViewModel

class NoteFragment : Fragment() {
    private var _binding: FragmentNoteBinding? = null
    private val binding get() = _binding!!
    private val noteViewModel by viewModels<NoteViewModel>()
//    private val noteAdapter = NoteAdapter(::navigate)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNoteBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            noteViewModel.titles.observe(viewLifecycleOwner) { titles ->
                val f: (NavDirections) -> Unit = findNavController()::navigate
                val g: (String) -> NavDirections =
                    { NoteFragmentDirections.actionNoteFragmentToLetterFragment(it) }
                val comp = compose(f, g)
                rvTitleList.adapter =
                    NoteAdapter(comp).apply {
                        addNote(titles)
                    }
            }

            btnAddTitle.setOnClickListener {
                noteViewModel.addTitles(tfTitle.text.toString())
            }
        }
    }
}


fun <A, B, C> compose(f: (B) -> C, g: (A) -> B): (A) -> C {
    return { f(g(it)) }
}

fun <B : NavDirections> compose2(f: (B) -> Unit, g: (String) -> B): (String) -> Unit = compose(f, g)

val <A:Int> A.add: Int
    get() = this + 6

//var Person.fullname: String
//    get() = "$first $last"
//    set(value) {
//        first = value.getFirst()
//        last = value.getLast()
//    }