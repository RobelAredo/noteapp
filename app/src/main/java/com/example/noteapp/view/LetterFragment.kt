package com.example.noteapp.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.noteapp.databinding.FragmentLetterBinding

class LetterFragment: Fragment() {
    private var _binding: FragmentLetterBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<LetterFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLetterBinding.inflate(inflater, container, false).also{
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvLetterList.adapter = LetterAdapter().apply{ addLetter(args.title.map{it.toString()}) }
    }
}