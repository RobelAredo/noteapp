package com.example.noteapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemTitleBinding

class NoteAdapter(private val navigate: (String)->Unit): RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    private var notes = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder =
        NoteViewHolder(ItemTitleBinding.inflate(LayoutInflater.from(parent.context), parent, false)).apply{
            itemView.setOnClickListener{ navigate(notes[adapterPosition]) }
        }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.loadTitle(note)
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    fun addNote(notes: MutableList<String>){
        this.notes = notes.toMutableList()
        notifyDataSetChanged()
    }

    class NoteViewHolder(private val binding: ItemTitleBinding): RecyclerView.ViewHolder(binding.root) {
        fun loadTitle(note: String){
            binding.cvItemNote.text = note.toString()
        }
    }
}