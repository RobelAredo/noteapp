package com.example.noteapp.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemTitleBinding

class LetterAdapter(): RecyclerView.Adapter<LetterAdapter.NoteViewHolder>() {

    private var notes = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder =
        NoteViewHolder(ItemTitleBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.loadLetter(note)
    }

    override fun getItemCount(): Int {
        return notes.size
    }

    fun addLetter(notes: List<String>){
        this.notes = notes.toMutableList()
        notifyDataSetChanged()
    }

    class NoteViewHolder(private val binding: ItemTitleBinding): RecyclerView.ViewHolder(binding.root) {
        fun loadLetter(letter: String){
            binding.cvItemNote.text = letter
        }
    }
}